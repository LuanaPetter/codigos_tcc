import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.svm import SVC
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import label_binarize
from sklearn.metrics import precision_recall_curve, average_precision_score
import pickle

sub = 'C'
subject = 'Sub%s_14chan_3LRR' % sub


filename = 'SVM_%s_X_test.pkl' % sub
X_test = pickle.load(open(filename, 'rb'))

filename = 'SVM_%s_y_test.pkl' % sub
y_test = pickle.load(open(filename, 'rb'))

#carrega o treino do pca
filename = 'SVM_%s_pca.pkl' % sub
pca = pickle.load(open(filename, 'rb'))

#carrega o treino do classificador
filename = 'SVM_%s_clf.pkl' % sub
clf = pickle.load(open(filename, 'rb'))

#passa o pca no X_test
X_t_test = pca.transform(X_test)

results = {}
predicted_classes = clf.predict(X_t_test) # nominal classes

# Confusion matrix
df = pd.DataFrame({'y_true': y_test, 'y_pred': predicted_classes})
df_confusion = pd.crosstab(df.y_true, df.y_pred, rownames=['Actual'], colnames=['Predicted'], margins=False)

print 'Confusion matrix'
print '-----------------------'
print df_confusion

TF =  np.float(df_confusion['left hand']['left hand']) # predicted foot / real foot
TH = np.float(df_confusion['right hand']['right hand']) # predicted hand / real hand
FF = np.float(df_confusion['right hand']['left hand']) # predicted foot / real hand
FH = np.float(df_confusion['left hand']['right hand']) # predicted hand / real foot

results['left_hand_precision'] = TF / (TF + FF)
results['right_hand_precision'] = TH / (TH + FH)
results['left_hand_recall'] = TF / (TF + FH)
results['right_hand_recall'] = TH / (TH + FF)

print 'left hand precision: ', results['left_hand_precision']
print 'right hand precision: ', results['right_hand_precision']
print 'left hand recall:', results['left_hand_recall']
print 'right hand recall:' , results['right_hand_recall']


T = clf.predict_proba(X_t_test)
y_pred = []

for i in range(len(T)):
    y_pred.append(T[i][0])
    
y_test_bin = label_binarize(y_test, classes = ['right hand', 'left hand'])

# Precision-Recall
RF_precision, RF_recall, _ = precision_recall_curve(y_true = y_test_bin, probas_pred = y_pred)
RF_auc_PR = average_precision_score(y_true = y_test_bin, y_score = y_pred)

print('Subject '+sub+' --- Scores With PCA and whitening')
print('-------------------------------------')
print('ACC = ', clf.score(X_t_test, y_test))     # Testa a acuracia das fases de treino e teste.
print('AUC ROC = ', roc_auc_score(y_test_bin, y_pred))
print('AUC PR = ', RF_auc_PR)

