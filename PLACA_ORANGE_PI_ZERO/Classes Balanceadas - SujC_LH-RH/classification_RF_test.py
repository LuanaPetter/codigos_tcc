from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
import pandas as pd
import numpy as np
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import classification_report
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.metrics import precision_recall_curve, average_precision_score
import pickle

sub = 'C'
subject = 'Sub%s_14chan_3LRR' % sub

#carrega X_test gerado no classification_RF_train.py para n precisar separar de novo
filename = 'RF_%s_X_test.pkl' % sub
X_test = pickle.load(open(filename, 'rb'))

filename = 'RF_%s_y_test.pkl' % sub
y_test = pickle.load(open(filename, 'rb'))

#carrega o treino do pca
filename = 'RF_%s_pca.pkl' % sub
pca = pickle.load(open(filename, 'rb'))

#carrega o treino do classificador
filename = 'RF_%s_clf.pkl' % sub
clf = pickle.load(open(filename, 'rb'))

#passa o pca no X_test
X_t_test = pca.transform(X_test)

results = {}
predicted_classes = clf.predict(X_t_test) # nominal classes

# Confusion matrix
df = pd.DataFrame({'y_true': y_test, 'y_pred': predicted_classes})
df_confusion = pd.crosstab(df.y_true, df.y_pred, rownames=['Actual'], colnames=['Predicted'], margins=False)

print 'Confusion matrix'
print '-----------------------'
print df_confusion

TF =  np.float(df_confusion['left hand']['left hand']) # predicted foot / real foot
TH = np.float(df_confusion['right hand']['right hand']) # predicted hand / real hand
FF = np.float(df_confusion['right hand']['left hand']) # predicted foot / real hand
FH = np.float(df_confusion['left hand']['right hand']) # predicted hand / real foot

results['left_hand_precision'] = TF / (TF + FF)
results['right_hand_precision'] = TH / (TH + FH)
results['left_hand_recall'] = TF / (TF + FH)
results['right_hand_recall'] = TH / (TH + FF)

print 'left hand precision: ', results['left_hand_precision']
print 'right hand precision: ', results['right_hand_precision']
print 'left hand recall:', results['left_hand_recall']
print 'right hand recall:' , results['right_hand_recall']
