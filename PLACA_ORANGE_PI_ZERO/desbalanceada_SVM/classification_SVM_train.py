import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.svm import SVC
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import label_binarize
import pickle

#Load dataset
subjects = ['A', 'B', 'C']
for sub in subjects:
	df = pd.read_csv('features-Sub%s_5chan_3LRF-MeanVarSkew-mu_and_beta.csv' % sub)
	columns = df.columns
	
	Data = []
	for i in range(np.shape(columns)[0] - 1): 
		Data.append(df['%s' % columns[i]])

	Data = np.transpose(Data)

	y = df['class']

	X_train, X_test, y_train, y_test = train_test_split(Data, y, test_size=0.4, random_state=0)
	
	# Binarize labels. It is needed for classification
	y_test_bin = label_binarize(y_test, classes=['hand', 'foot'])
	
	# Save model to a pickle file
	filename = 'SVM_%s_X_test.pkl' % sub
	pickle.dump(X_test, open(filename, 'wb'))

	# Save model to a pickle file
	filename = 'SVM_%s_y_test.pkl' % sub
	pickle.dump(y_test, open(filename, 'wb'))
	
	#apply PCA and Whitgening --- Optimizing parameters with GridSearch
	pca = PCA(n_components=30, copy=True, whiten=True, svd_solver='full', tol=0.0, iterated_power='auto', 
          random_state=1)
	pca.fit(X_train)
	X_t_train = pca.transform(X_train)
	X_t_test = pca.transform(X_test)

	# Save model to a pickle file
	filename = 'SVM_%s_pca.pkl' % sub
	pickle.dump(pca, open(filename, 'wb'))
	
	possible_parameters = {'C': np.logspace(2, 6, 5),
                       'gamma': np.logspace(-5, 0, 5)}
	
	svm = SVC(kernel='rbf', probability=True, tol=0.001, class_weight='balanced', max_iter=-1, decision_function_shape=None,
			  random_state=1)
	clf = GridSearchCV(svm, possible_parameters, cv = 5)
	clf = clf.fit(X_t_train, y_train);
	
	print('BEST PARAMETERS: ', clf.best_params_)
	
	# Save model to a pickle file
	filename = 'SVM_%s_clf.pkl' % sub
	pickle.dump(clf, open(filename, 'wb')) # Gera arquivo com o aprendizado do classificador.
	
	#Performance measures
	T = clf.predict_proba(X_t_test)
	y_pred = []

	for i in range(len(T)):
		y_pred.append(T[i][0])
		
	y_test_bin = label_binarize(y_test, classes=['hand', 'foot'])
	print('')
	print('Subject '+sub)
	print('Scores with PCA and Third Set of Best Parameters')
	print('-------------------------------------')
	print('ACC = ', clf.score(X_t_test, y_test))
	print('AUC = ', roc_auc_score(y_test_bin, y_pred))
	