
# coding: utf-8

# In[1]:

import pandas as pd
from scipy.stats import skew, kurtosis
import numpy as np
from scipy.linalg import inv
from scipy.signal import butter, filtfilt
import scipy.io as sio


# In[2]:

sub = 'B'
subject = 'Sub%s_6chan_2LR' % sub


# In[3]:

mat = sio.loadmat(subject + '.mat')

data = mat['EEGDATA']
labels = mat['LABELS']
info = mat['Info']

ntrials = len(labels)
len_trial = len(data[0,:,0])
fs = info[0][0][0][0][0]


# In[4]:

len_trial


# In[5]:

mrk = pd.DataFrame(columns = ['class'])
aux = []

for lab in labels:

    if lab == [1]:
        aux.append('left hand')
    elif lab == [2]:
        aux.append('right hand')

mrk['class'] = aux 


# In[6]:

channels = ['C3', 'Cp3', 'C4', 'Cp4', 'Cz']
dataset = pd.DataFrame(columns = channels)

aux1 = []; aux2 = []; aux3 = []; aux4 = []; aux5 = [];

for trial in range(ntrials):
        # the function .extend is used (and not .append()) because each 'data[chan, :, trial]' is a list
        # if you have a = [1,2,3]; b = [4,5,6]; c = [7,8,9]
        # .append() => [[1,2,3], [4,5,6], [7,8,9]]
        # .extend() => [1,2,3,4,5,6,7,8,9]
    aux1.extend(data[0, :, trial])
    aux2.extend(data[1, :, trial])
    aux3.extend(data[2, :, trial])
    aux4.extend(data[3, :, trial])
    aux5.extend(data[4, :, trial])

dataset['C3'] = aux1
dataset['Cp3'] = aux2
dataset['C4'] = aux3
dataset['Cp4'] = aux4
dataset['Cz'] = aux5


# In[7]:

fnyq = fs/2

# Mu band
filt_sig_mu = pd.DataFrame(columns = channels)
lowfq_mu = 9.
highfq_mu = 12.

for chan in channels:
    b1, a1 = butter(3, [lowfq_mu/fnyq, highfq_mu/fnyq], btype = 'band', output = 'ba')
    filt_sig_mu[chan] = filtfilt(b1, a1, dataset[chan])

# Beta band
lowfq_beta = 16.
highfq_beta = 31.
filt_sig_beta = pd.DataFrame(columns = channels)

for chan in channels:
    b2, a2 = butter(3, [lowfq_beta/fnyq, highfq_beta/fnyq], btype = 'band', output = 'ba')
    filt_sig_beta[chan] = filtfilt(b2, a2, dataset[chan]) 


# In[8]:

features = pd.DataFrame(columns = ['mean_C3_mu', 'mean_Cp3_mu', 'mean_C4_mu', 'mean_Cp4_mu', 'mean_Cz_mu',
                                       'var_C3_mu', 'var_Cp3_mu', 'var_C4_mu', 'var_Cp4_mu', 'var_Cz_mu',
                                       'skew_C3_mu', 'skew_Cp3_mu', 'skew_C4_mu', 'skew_Cp4_mu', 'skew_Cz_mu',
                                       'mean_C3_beta', 'mean_Cp3_beta', 'mean_C4_beta', 'mean_Cp4_beta', 'mean_Cz_beta',
                                       'var_C3_beta', 'var_Cp3_beta', 'var_C4_beta', 'var_Cp4_beta', 'var_Cz_beta',
                                       'skew_C3_beta', 'skew_Cp3_beta', 'skew_C4_beta', 'skew_Cp4_beta', 'skew_Cz_beta', 'class'])

for chan in channels:
    mn_mu = []; vr_mu = []; sk_mu = [];
    mn_beta = []; vr_beta = []; sk_beta = [];

    for trial in range(ntrials):
        aux_mu = []; aux_beta = []

        for i in range(len_trial):
            aux_mu.append(filt_sig_mu[chan][len_trial*trial+i])
            aux_beta.append(filt_sig_beta[chan][len_trial*trial+i])

        mn_mu.append(np.mean(aux_mu))
        vr_mu.append(np.var(aux_mu))
        sk_mu.append(skew(aux_mu))

        mn_beta.append(np.mean(aux_beta))
        vr_beta.append(np.var(aux_beta))
        sk_beta.append(skew(aux_beta))

    features['mean_%s_mu' % (chan)] = mn_mu
    features['var_%s_mu' % (chan)] = vr_mu
    features['skew_%s_mu' % (chan)] = sk_mu
    features['mean_%s_beta' % (chan)] = mn_beta
    features['var_%s_beta' % (chan)] = vr_beta
    features['skew_%s_beta' % (chan)] = sk_beta

features['class'] = mrk   


# In[9]:

features.to_csv('features-%s-MeanVarSkew-mu_and_beta.csv' % (sub), index = False, encoding='utf-8')


# In[ ]:



