
# coding: utf-8

# In[1]:

from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
import pandas as pd
import numpy as np
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import classification_report
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.metrics import precision_recall_curve, average_precision_score
import pickle


# In[2]:

sub = 'B'
subject = 'Sub%s_5chan_3LRR' % sub


# In[3]:

#carrega X_test gerado no classification_RF_train.py para n precisar separar de novo
filename = 'RF_%s_X_test.pkl' % sub
X_test = pickle.load(open(filename, 'rb'))

filename = 'RF_%s_y_test.pkl' % sub
y_test = pickle.load(open(filename, 'rb'))


# In[4]:

#carrega o treino do pca
filename = 'RF_%s_pca.pkl' % sub
pca = pickle.load(open(filename, 'rb'))


# In[5]:

#carrega o treino do classificador
filename = 'RF_%s_clf.pkl' % sub
clf = pickle.load(open(filename, 'rb'))


# In[6]:

#passa o pca no X_test
X_t_test = pca.transform(X_test)

results = {}
predicted_classes = clf.predict(X_t_test) # nominal classes


# In[7]:

# Confusion matrix
df = pd.DataFrame({'y_true': y_test, 'y_pred': predicted_classes})
df_confusion = pd.crosstab(df.y_true, df.y_pred, rownames=['Actual'], colnames=['Predicted'], margins=False)

print 'Confusion matrix'
print '-----------------------'
print df_confusion

TF =  np.float(df_confusion['left hand']['left hand']) # predicted foot / real foot
TH = np.float(df_confusion['right hand']['right hand']) # predicted hand / real hand
FF = np.float(df_confusion['right hand']['left hand']) # predicted foot / real hand
FH = np.float(df_confusion['left hand']['right hand']) # predicted hand / real foot

results['left_hand_precision'] = TF / (TF + FF)
results['right_hand_precision'] = TH / (TH + FH)
results['left_hand_recall'] = TF / (TF + FH)
results['right_hand_recall'] = TH / (TH + FF)

print 'left hand precision: ', results['left_hand_precision']
print 'right hand precision: ', results['right_hand_precision']
print 'left hand recall:', results['left_hand_recall']
print 'right hand recall:' , results['right_hand_recall']


# In[8]:

#Performance measures --- Ja executado no classification_RF_Train.py
T = clf.predict_proba(X_t_test)
y_pred = []

for i in range(len(T)):
    y_pred.append(T[i][0])
    
y_test_bin = label_binarize(y_test, classes = ['right hand', 'left hand'])
#y_test_bin = label_binarize(y_test, classes = ['left hand', 'right hand'])


# In[9]:

# Precision-Recall
RF_precision, RF_recall, _ = precision_recall_curve(y_true = y_test_bin, probas_pred = y_pred)
RF_auc_PR = average_precision_score(y_true = y_test_bin, y_score = y_pred)

print('Subject '+sub+' --- Scores With PCA and whitening')
print('-------------------------------------')
print('ACC = ', clf.score(X_t_test, y_test))     # Testa a acuracia das fases de treino e teste.
print('AUC ROC = ', roc_auc_score(y_test_bin, y_pred))
print('AUC PR = ', RF_auc_PR)


# In[10]:

'''#Salvando dados --> ROC
RF_fpr, RF_tpr, _ = roc_curve(y_true = y_test_bin, y_score = y_pred)
RF_roc_auc = roc_auc_score(y_test_bin, y_pred);

results_roc = {}

#Save results into a pickle file
results_roc['RF_fpr'] = RF_fpr
results_roc['RF_tpr'] = RF_tpr
results_roc['RF_roc_auc'] = RF_roc_auc

filename = 'results_roc_RF_B.pkl'
pickle.dump(results_roc, open(filename, 'wb'))

#Salvando dados --> PR
RF_precision, RF_recall, _ = precision_recall_curve(y_true = y_test_bin, probas_pred = y_pred)
RF_auc_PR = average_precision_score(y_true = y_test_bin, y_score = y_pred)

results_pr = {}

#Save results into a pickle file
results_pr['RF_recall'] = RF_recall
results_pr['RF_precision'] = RF_precision
results_pr['RF_auc_PR'] = RF_auc_PR

filename = 'results_pr_RF_B.pkl' 
pickle.dump(results_pr, open(filename, 'wb'))'''


# In[ ]:



