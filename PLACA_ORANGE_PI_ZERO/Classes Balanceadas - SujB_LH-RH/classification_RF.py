
# coding: utf-8

# In[1]:

# Load libraries
import pandas as pd #
from scipy.signal import butter, filtfilt
from scipy.stats import skew
import numpy as np
from datetime import datetime
from sklearn.decomposition import PCA
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.metrics import precision_recall_curve, average_precision_score
from sklearn.preprocessing import label_binarize
from sklearn.ensemble import RandomForestClassifier
import time
import sys
import scipy.io as sio
import pickle, json


# In[2]:

sub = 'B'
subject = 'Sub%s_6chan_2LR' % sub


# In[3]:

# Load matfile
# Downloaded from http://www.bsp.brain.riken.jp/~qibin/homepage/Datasets.html
mat = sio.loadmat(subject + '.mat')

# Separate the matfile into variables containing different informations
data = mat['EEGDATA']
labels = mat['LABELS']
info = mat['Info']

ntrials = len(labels) #270
len_trial = len(data[0,:,0]) #1024
fs = info[0][0][0][0][0]


# In[4]:

#Adequacao das classes

aux = []

for lab in labels:

    if lab == [1]:
        aux.append('left hand')
    elif lab == [2]:
        aux.append('right hand')


# In[5]:

#carrega y_test gerado no classification_RF_train.py para saber quais as trials do conj de testes
'''filename = 'RF_%s_X_test.pkl' % sub
X_test = pickle.load(open(filename, 'rb'))'''

filename = 'RF_%s_y_test.pkl' % sub
y_test = pickle.load(open(filename, 'rb'))


#lista com os Indices do conjunto de testes
val=[]
val = sorted(y_test.index) #pega os indices do y_test em ordem


# In[6]:

#channels = ['C3', 'Cp3', 'C4', 'Cp4', 'Cz'] 

data_test = pd.DataFrame(columns = ['mean_C3_mu', 'mean_Cp3_mu', 'mean_C4_mu', 'mean_Cp4_mu', 'mean_Cz_mu',
                                   'var_C3_mu', 'var_Cp3_mu', 'var_C4_mu', 'var_Cp4_mu', 'var_Cz_mu',
                                   'skew_C3_mu', 'skew_Cp3_mu', 'skew_C4_mu', 'skew_Cp4_mu', 'skew_Cz_mu',
                                   'mean_C3_beta', 'mean_Cp3_beta', 'mean_C4_beta', 'mean_Cp4_beta', 'mean_Cz_beta',
                                   'var_C3_beta', 'var_Cp3_beta', 'var_C4_beta', 'var_Cp4_beta', 'var_Cz_beta',
                                   'skew_C3_beta', 'skew_Cp3_beta', 'skew_C4_beta', 'skew_Cp4_beta', 'skew_Cz_beta'])


# In[7]:

#Carrega treino do pca e do classificador
filename = 'RF_%s_pca.pkl' % sub
pca = pickle.load(open(filename, 'rb'))

filename = 'RF_%s_clf.pkl' % sub
clf = pickle.load(open(filename, 'rb'))


# In[8]:

results = {}
predicted_classes = [] #Para ir salvando cada predicao
y_esperada=[]
X_total=[] 


# In[9]:

ntrials


# In[10]:

#----------------- LE UM TRIAL POR VEZ -------------------- Composto por 1024 pontos um trial --> 256Hz * 4s
start_t = time.time()
index=0
T=0 # conta as trials --- TIRAR DEPOIS
channels = ['C3', 'Cp3', 'C4', 'Cp4', 'Cz'] 



for trial in range(ntrials):
    if(trial == val[index]):
        print('Trial %d :'%T)
        start = time.time() #tempo e iniciado a cada trial
        

        tentativa = pd.DataFrame(columns = channels) #P zerar a cada nova trial

        aux1 = []; aux2 = []; aux3 = []; aux4 = []; aux5 = []; #limpa as listas para cada trial
        #listas que vao guardar os 1024 pontos de cada tentativa
        aux1.extend(data[0, :, trial])
        aux2.extend(data[1, :, trial])
        aux3.extend(data[2, :, trial])
        aux4.extend(data[3, :, trial])
        aux5.extend(data[4, :, trial])
        #cada lista guardou os 1024 pontos de uma tentativa, para cada canal


        #if (trial == 0): #TIRAR DEPOIS, PARA QUE POSSA PEGAR TODAS AS TRIALS
        #para o canal 0 =  C3
        tentativa['C3'] = aux1
        #para o canal 1 =  Cp3           
        tentativa['Cp3'] = aux2
        #para o canal 2 =  C4  
        tentativa['C4'] = aux3
        #para o canal 3 =  Cp4  
        tentativa['Cp4'] = aux4
        #para o canal 4 =  C4
        tentativa['Cz'] = aux5


        #para cada trial faz os calculos

        ##--------------------------------- FILTRO ------------------------------ MU E BETA
        # FIlter dataset into MU and BETA frequency bands
        fnyq = fs/2
        filter_order = 3
        columns = []
        [columns.append(tentativa.columns[:][i]) for i in range(len(tentativa.columns))] #5 colunas --- cada canal

        # Mu band
        filt_sig_mu = pd.DataFrame(columns = columns[:])
        lowfq_mu = 9.
        highfq_mu = 12.

        for chan in columns[:]:
            b1, a1 = butter(filter_order, [lowfq_mu/fnyq, highfq_mu/fnyq], btype = 'band', output = 'ba')
            filt_sig_mu[chan] = filtfilt(b1, a1, tentativa[chan])


        # Beta band
        lowfq_beta = 16.
        highfq_beta = 31.
        filt_sig_beta = pd.DataFrame(columns = columns[:])

        for chan in columns[:]:
            b2, a2 = butter(filter_order, [lowfq_beta/fnyq, highfq_beta/fnyq], btype = 'band', output = 'ba')
            filt_sig_beta[chan] = filtfilt(b2, a2, tentativa[chan])


        ##--------------------------------- Features ------------------------------ 
        # Define the name of columns.
        features = pd.DataFrame(columns = ['mean_C3_mu', 'mean_Cp3_mu', 'mean_C4_mu', 'mean_Cp4_mu', 'mean_Cz_mu',
                                       'var_C3_mu', 'var_Cp3_mu', 'var_C4_mu', 'var_Cp4_mu', 'var_Cz_mu',
                                       'skew_C3_mu', 'skew_Cp3_mu', 'skew_C4_mu', 'skew_Cp4_mu', 'skew_Cz_mu',
                                       'mean_C3_beta', 'mean_Cp3_beta', 'mean_C4_beta', 'mean_Cp4_beta', 'mean_Cz_beta',
                                       'var_C3_beta', 'var_Cp3_beta', 'var_C4_beta', 'var_Cp4_beta', 'var_Cz_beta',
                                       'skew_C3_beta', 'skew_Cp3_beta', 'skew_C4_beta', 'skew_Cp4_beta', 'skew_Cz_beta'])
        # Extract mean, var and skew features from 4 second size windows
        for chan in channels:
            mn_mu = []; vr_mu = []; sk_mu = [];
            mn_beta = []; vr_beta = []; sk_beta = [];


            aux_mu = []; aux_beta = []

                # Separate the trials and save it into aux_beta and aux_mu, each one corresponding to its own frequency band
                # This way is better for modifications.. when you want to decrease or increase the window lenght to calculate the features from.
            for i in range(len_trial):
                aux_mu.append(filt_sig_mu[chan][i])
                aux_beta.append(filt_sig_beta[chan][i])

            mn_mu.append(np.mean(aux_mu))
            vr_mu.append(np.var(aux_mu))
            sk_mu.append(skew(aux_mu))

            mn_beta.append(np.mean(aux_beta))
            vr_beta.append(np.var(aux_beta))
            sk_beta.append(skew(aux_beta))

            features['mean_%s_mu' % (chan)] = mn_mu
            features['var_%s_mu' % (chan)] = vr_mu
            features['skew_%s_mu' % (chan)] = sk_mu
            features['mean_%s_beta' % (chan)] = mn_beta
            features['var_%s_beta' % (chan)] = vr_beta
            features['skew_%s_beta' % (chan)] = sk_beta
            
            
            
            
        
        
        if(index<64): #65
            index=index+1
            
        
        #Achou uma featur do conjunto de testes
        
        time_of_extraction = time.time() - start
        print  'Tempo de extracao de features da trial %d: '%T, time_of_extraction
    
        time_star_classification = time.time()
        
        ##--------------------------------- CLASSIFICAcaO ------------------------------ 
        # Transform dataframe into arrays for 'train_test_split' method
        time_4 = time.time()
        Data = []
        for i in range(np.shape(features.columns)[0]):
            Data.append(features['%s' % features.columns[i]])
        Data = np.transpose(Data)
        
        X_t_test = pca.transform(Data) # Passa o pca na trial
    
        X_total.extend(X_t_test) #guarda todos X_t_test
        
        
        y_esperada.extend(aux[T:T+1]) #guarda qual era a tag esperada
    
        predicted_classes.extend(clf.predict(X_t_test)) #guarda a tag de cada predicao


        print('Tag esperada %s  ---- Tag predita %s'%(aux[T], clf.predict(X_t_test)))


        time_stop_classification = time.time() - time_star_classification
        print 'Tempo de classificacao da trial %d: '%T, time_stop_classification

        print 'Tempo TOTAL para a trial %d: '%T, time_stop_classification+time_of_extraction

        print('')
        
        
               
        
   
    
    T=T+1 # Total de trials do sujeito    
print T


# In[11]:

# Confusion matrix
df = pd.DataFrame({'y_true': y_esperada, 'y_pred': predicted_classes})
df_confusion = pd.crosstab(df.y_true, df.y_pred, rownames=['Actual'], colnames=['Predicted'], margins=False)
print 'Confusion matrix'
print '-----------------------'
print df_confusion
# print '\nConfusion matrix in LaTeX'
# print '-----------------------'
# print df_confusion.to_latex()

TF =  np.float(df_confusion['left hand']['left hand']) # predicted foot / real foot
TH = np.float(df_confusion['right hand']['right hand']) # predicted hand / real hand
FF = np.float(df_confusion['right hand']['left hand']) # predicted foot / real hand
FH = np.float(df_confusion['left hand']['right hand']) # predicted hand / real foot

results['left_hand_precision'] = TF / (TF + FF)
results['right_hand_precision'] = TH / (TH + FH)
results['left_hand_recall'] = TF / (TF + FH)
results['right_hand_recall'] = TH / (TH + FF)

print 'left hand precision: ', results['left_hand_precision']
print 'right hand precision: ', results['right_hand_precision']
print 'left hand recall:', results['left_hand_recall']
print 'right hand recall:' , results['right_hand_recall']



#X_total --Lista de array
myarray = np.asarray(X_total) #converte a lista de array em um array


T = clf.predict_proba(myarray)
y_pred = []

for i in range(len(T)):
    y_pred.append(T[i][0])
    
y_test_bin = label_binarize(y_esperada, classes = ['right hand', 'left hand'])


# Precision-Recall
RF_precision, RF_recall, _ = precision_recall_curve(y_true = y_test_bin, probas_pred = y_pred)
RF_auc_PR = average_precision_score(y_true = y_test_bin, y_score = y_pred)

print('Scores With PCA and whitening')
print('-------------------------------------')
print('ACC = ', clf.score(myarray, y_esperada))
print('AUC ROC = ', roc_auc_score(y_test_bin, y_pred))
print('AUC PR = ', RF_auc_PR)

# Total time running
print '\nTotal time of execution: ', time.time()-start_t, 'seconds.'

# In[13]:

#Salvando dados --> ROC
RF_fpr, RF_tpr, _ = roc_curve(y_true = y_test_bin, y_score = y_pred)
RF_roc_auc = roc_auc_score(y_test_bin, y_pred);

results_roc = {}

#Save results into a pickle file
results_roc['RF_fpr'] = RF_fpr
results_roc['RF_tpr'] = RF_tpr
results_roc['RF_roc_auc'] = RF_roc_auc

filename = 'results_roc_RF_B.pkl'
pickle.dump(results_roc, open(filename, 'wb'))

#Salvando dados --> PR
RF_precision, RF_recall, _ = precision_recall_curve(y_true = y_test_bin, probas_pred = y_pred)
RF_auc_PR = average_precision_score(y_true = y_test_bin, y_score = y_pred)

results_pr = {}

#Save results into a pickle file
results_pr['RF_recall'] = RF_recall
results_pr['RF_precision'] = RF_precision
results_pr['RF_auc_PR'] = RF_auc_PR

filename = 'results_pr_RF_B.pkl' 
pickle.dump(results_pr, open(filename, 'wb'))


# In[ ]:




# In[ ]:



