from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import classification_report
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.metrics import precision_recall_curve, average_precision_score
import pickle

#Load dataset
subjects = ['A', 'B', 'C']
for sub in subjects:
	df = pd.read_csv('features-Sub%s_5chan_3LRF-MeanVarSkew-mu_and_beta.csv' % sub)
	columns = df.columns

	Data = []
	for i in range(np.shape(columns)[0] - 1): 
		Data.append(df['%s' % columns[i]])

	Data = np.transpose(Data)
	
	y = df['class']

	X_train, X_test, y_train, y_test = train_test_split(Data, 
														y, 
														test_size=0.4, 
														random_state=0)
	#Save model to a pickle file
	filename = 'RF_%s_X_test.pkl' % sub
	pickle.dump(X_test, open(filename, 'wb'))

	# Save model to a pickle file
	filename = 'RF_%s_y_test.pkl' % sub
	pickle.dump(y_test, open(filename, 'wb'))
	
	#apply PCA and Whitgening

	
	pca = PCA(n_components=30,
                  copy=True,
                  whiten=True,
                  svd_solver='full',
				  #tol=0.0,
                  iterated_power='auto',
                  random_state=1)
	pca.fit(X_train)
	X_t_train = pca.transform(X_train)
	X_t_test = pca.transform(X_test)
	
	# Save model to a pickle file
	filename = 'RF_%s_pca.pkl' % sub
	pickle.dump(pca, open(filename, 'wb'))

	#Random ForestClassifier
	clf = RandomForestClassifier(n_estimators=10,
								 min_samples_split=6,
								 min_samples_leaf=3, 
								 max_features='auto',
								 class_weight='balanced')

	clf.fit(X_t_train, y_train);
	# Save model to a pickle file
	filename = 'RF_%s_clf.pkl' % sub
	pickle.dump(clf, open(filename, 'wb')) # Gera arquivo com o aprendizado do classificador.
	